from django.urls import path
from django.conf.urls import url
from .views import *
app_name = 'homepage'

urlpatterns = [
	url(r"^deleteurl/(?P<delete_id>\d+)/$",delete, name='delete'),
    path('', index, name='index'),
    path('aboutme/',aboutme, name='aboutme'),
    path('myhooby/', myhooby, name='myhooby'),
    path('contact/', contact, name='contact'),
    path('resume/', resume, name='resume'),
	path('schedule/',schedule, name='schedule'),
    path('schedule/create', schedule_create, name='schedule_create'),
    path('schedule/delete', schedule_delete, name='schedule_delete'),

]